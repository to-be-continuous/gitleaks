# [2.7.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.6.1...2.7.0) (2025-01-27)


### Features

* disable tracking service by default ([8d614e4](https://gitlab.com/to-be-continuous/gitleaks/commit/8d614e42b2c9747e6e1d816ffc80c19b165e2364))

## [2.6.1](https://gitlab.com/to-be-continuous/gitleaks/compare/2.6.0...2.6.1) (2024-09-23)


### Bug Fixes

* detect subcommand is now deprecated ([9860a4c](https://gitlab.com/to-be-continuous/gitleaks/commit/9860a4c2d0096ef3e647b2051fd9d2410a0f1a53))

# [2.6.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.5.2...2.6.0) (2024-07-15)


### Features

* default gitleaks arguments to avoid secret exposure ([595fc7d](https://gitlab.com/to-be-continuous/gitleaks/commit/595fc7dfe42fd00c8c5dc6647779bed24955e0f4))

## [2.5.2](https://gitlab.com/to-be-continuous/gitleaks/compare/2.5.1...2.5.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([3d43c7f](https://gitlab.com/to-be-continuous/gitleaks/commit/3d43c7f96d6e623bf20df17f0302b2dbe22b6fa2))

## [2.5.1](https://gitlab.com/to-be-continuous/gitleaks/compare/2.5.0...2.5.1) (2024-03-31)


### Bug Fixes

* **verbose:** restore verbose by default as otherwise found secrets aren't printed in the console ([367a84c](https://gitlab.com/to-be-continuous/gitleaks/commit/367a84ce6548f0c529e7b555ca16a821474c3472))

# [2.5.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.4.1...2.5.0) (2024-03-31)


### Features

* **verbose:** verbose is no longer the default ([4db5301](https://gitlab.com/to-be-continuous/gitleaks/commit/4db5301a24cb7f7f87d040be3e3234d13b4b7754)), closes [#17](https://gitlab.com/to-be-continuous/gitleaks/issues/17)

## [2.4.1](https://gitlab.com/to-be-continuous/gitleaks/compare/2.4.0...2.4.1) (2024-1-31)


### Bug Fixes

* remove install_ca_certs ([86464e6](https://gitlab.com/to-be-continuous/gitleaks/commit/86464e616d6b6db98df659af8ff90e9554bfdeb9))

# [2.4.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.3.0...2.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([e15efce](https://gitlab.com/to-be-continuous/gitleaks/commit/e15efce410230f21d5e5eba052413996d832559d))

# [2.3.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.2.3...2.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([37ccee8](https://gitlab.com/to-be-continuous/gitleaks/commit/37ccee878bf8f24374d6179a0716ca76e8935601))

## [2.2.3](https://gitlab.com/to-be-continuous/gitleaks/compare/2.2.2...2.2.3) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([df104cc](https://gitlab.com/to-be-continuous/gitleaks/commit/df104cc850da4d256e933d40db2c692283dfe056))

## [2.2.2](https://gitlab.com/to-be-continuous/gitleaks/compare/2.2.1...2.2.2) (2023-08-16)


### Bug Fixes

* **rules:** always use default config file ([91587a3](https://gitlab.com/to-be-continuous/gitleaks/commit/91587a3843fca87a87a20d4cf4319b39d5b0c871))

## [2.2.1](https://gitlab.com/to-be-continuous/gitleaks/compare/2.2.0...2.2.1) (2023-08-04)


### Bug Fixes

* **report:** normalize Gitleaks report ([12699e7](https://gitlab.com/to-be-continuous/gitleaks/commit/12699e718a49dd0412dd3ad3abce58aead6552f8))

# [2.2.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.1.1...2.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([a4b454e](https://gitlab.com/to-be-continuous/gitleaks/commit/a4b454e27fb378b181b3051eb6d463c9b82a9323))

## [2.1.1](https://gitlab.com/to-be-continuous/gitleaks/compare/2.1.0...2.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([47f16fd](https://gitlab.com/to-be-continuous/gitleaks/commit/47f16fd5eb3f71532b477fcb8a1ed1082cb8c31a))

# [2.1.0](https://gitlab.com/to-be-continuous/gitleaks/compare/2.0.0...2.1.0) (2022-10-05)


### Features

* trigger gitleaks jobs without any needs leveraging GitLab CI DAG ([bbf166f](https://gitlab.com/to-be-continuous/gitleaks/commit/bbf166f47785df0bacb115f47e2cbf56330c8bce))

# [2.0.0](https://gitlab.com/to-be-continuous/gitleaks/compare/1.3.0...2.0.0) (2022-08-05)


### Features

* adaptive pipeline ([0b07845](https://gitlab.com/to-be-continuous/gitleaks/commit/0b07845e0db777c77bd448ced1dacf03f16cd996))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.3.0](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.5...1.3.0) (2022-05-01)


### Features

* configurable tracking image ([f6e71c9](https://gitlab.com/to-be-continuous/gitleaks/commit/f6e71c9e274588615a120aae08fdfdac6c251f02))

## [1.2.5](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.4...1.2.5) (2022-04-19)


### Bug Fixes

* add build dir in safe.directory ([3271704](https://gitlab.com/to-be-continuous/gitleaks/commit/3271704f698ee8ca91177394bfd7f255d8a1cee2))

## [1.2.4](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.3...1.2.4) (2021-12-07)


### Bug Fixes

* replace obsolete --config-path= args by --config in gitleaks rules ([577e0c8](https://gitlab.com/to-be-continuous/gitleaks/commit/577e0c8b595d4e5ffa939a745a4ae2f5e7d9c775))

## [1.2.3](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.2...1.2.3) (2021-12-02)


### Bug Fixes

* update command options to be compliant with latest Gitleaks version ([59d39b7](https://gitlab.com/to-be-continuous/gitleaks/commit/59d39b707376cd82803a83370a0961dca12a55f8))

## [1.2.2](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.1...1.2.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([af9620f](https://gitlab.com/to-be-continuous/gitleaks/commit/af9620fb9716383ddb5005d42a9bf64a9f4be194))

## [1.2.1](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([f0ad30e](https://gitlab.com/to-be-continuous/gitleaks/commit/f0ad30e5fd81cccaace51201d5d68a7979cd7ef3))

## [1.2.0](https://gitlab.com/to-be-continuous/gitleaks/compare/1.1.1...1.2.0) (2021-06-10)

### Features

* move group ([554f528](https://gitlab.com/to-be-continuous/gitleaks/commit/554f52851c99aefa8338b9ef59e6476bd35c4407))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/compare/1.1.0...1.1.1) (2021-06-04)

### Bug Fixes

* force clone full commits history in gitleaks complete analysis ([0b2c51e](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/commit/0b2c51ef5d9cbb2794073939c2c18e9a457e5b66))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([9b701e5](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/commit/9b701e5c8c88881aaba689696810978e43abe7ea))

## 1.0.0 (2021-05-06)

### Features

* initial release ([fe810ba](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/commit/fe810ba058b4cac55d5bf4ccb342e6e5945d2045))
