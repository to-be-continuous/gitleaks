# GitLab CI template for Gitleaks

This project implements a GitLab CI/CD template to detect and prevent hardcoded secrets in your 
Git repository with [Gitleaks](https://github.com/zricethezav/gitleaks/wiki).

When run on the master branch, Gitleaks will audit all commits from all branches.

When run on other branches, Gitleaks will run a quick analysis on the current branch.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/gitleaks/gitlab-ci-gitleaks@2.7.0
    # 2: set/override component inputs
    inputs:
      args: "--no-banner" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/gitleaks'
    ref: '2.7.0'
    file: '/templates/gitlab-ci-gitleaks.yml'

variables:
  # 2: set/override template variables
  GITLEAKS_ARGS: "--no-banner" # ⚠ this is only an example
```

## `gitleaks` jobs configuration

Those jobs trigger a Gitleaks analysis (either on the complete repository, either on the current branch).
They use the following configuration.

| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `image` / `GITLEAKS_IMAGE` | The Docker image used to run Gitleaks  | `registry.hub.docker.com/zricethezav/gitleaks:latest` <br/>[![Trivy Badge](https://to-be-continuous.gitlab.io/doc/secu/trivy-badge-GITLEAKS_IMAGE.svg)](https://to-be-continuous.gitlab.io/doc/secu/trivy-GITLEAKS_IMAGE) |
| `rules` / `GITLEAKS_RULES` | Gitleaks [configuration rules](https://github.com/zricethezav/gitleaks#configuration) to use (you may also provide your own `.gitleaks.toml` configuration file in your project). | _none_ (uses default rules) |
| `args` / `GITLEAKS_ARGS` | [Options](https://github.com/zricethezav/gitleaks/wiki/Options) for a full Gitleaks analysis (on master or develop branches) | `--verbose --redact` |

### Configuring Gitleaks rules

Here is how this GitLab CI template chooses the Gitleaks rules to use:

1. It first looks for a `.gitleaks.toml` file at the root of your project repository.
2. If not found, it uses the `.toml` file specified with the `$GITLEAKS_RULES` variable.
3. If not specified, [default Gitleaks rules](https://github.com/zricethezav/gitleaks/blob/master/config/gitleaks.toml) are used.


Your Gitleaks rule file can extend the default rules by using `[extend]` + `useDefault = true` as explained in the [Gitleaks documentation](https://github.com/gitleaks/gitleaks/blob/master/README.md#configuration)
